<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\RegexForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Regex';
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>

<div class="row">
    <div class="col-lg-5">

        <?php $form = ActiveForm::begin(['id' => 'regex-form']); ?>

            <?= $form->field($model, 'input')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'intermediate')->textInput(['readonly' => true]) ?>

            <?= $form->field($model, 'output')->textInput(['readonly' => true]) ?>

            <div class="form-group">
                <?= Html::submitButton('Process', ['class' => 'btn btn-primary', 'name' => 'regex-button']) ?>
            </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
