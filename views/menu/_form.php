<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MenuItem */
/* @var $form ActiveForm */
/* @var $menu array */
?>
<div class="menu-_form">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'title') ?>
        <?= $form->field($model, 'link') ?>
        <?= $form->field($model, 'parent_id')->dropDownList($menu, [
            'options' => [
                $model->id => [
                    'disabled' => true,
                ],
            ],
        ]) ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- menu-_form -->
