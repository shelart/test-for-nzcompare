<?php

/* @var $this yii\web\View */
/* @var $model app\models\MenuItem */
/* @var $menu array */

$this->title = 'Create menu item';
?>

<h1><?=$this->title?></h1>

<?=$this->render('_form', [
    'model' => $model,
    'menu' => $menu,
])?>

