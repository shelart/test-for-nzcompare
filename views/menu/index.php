<?php

/* @var $this yii\web\View */
/* @var $menu array */

$this->title = 'Menu';
?>

<h1>Menu</h1>

<a class="btn btn-success" href="<?=Yii::$app->urlManager->createUrl(['/menu/create'])?>">Add root item</a>
<?php echo $this->render('_submenu', ['menu' => $menu]); ?>
