<?php
use yii\helpers\Html;
use richardfan\widget\JSRegister;
/* @var $this yii\web\View */
/* @var $menu array */
?>

<ul>
    <?php foreach ($menu as $item): ?>
    <li>
        <?= Html::a($item['title'], $item['link']) ?>
        (ID: <?=$item['id']?>)
        <a class="btn btn-info" href="<?=Yii::$app->urlManager->createUrl(['menu/update', 'id' => $item['id']])?>">Edit</a>
        <a class="btn btn-success" href="<?=Yii::$app->urlManager->createUrl(['menu/create', 'parentID' => $item['id']])?>">Add subitem</a>
        <?= Html::beginForm(['menu/delete', 'id' => $item['id']], 'post', ['style' => 'display: inline-block;', 'class' => 'frmDelete']) ?>
            <?= Html::submitButton('Delete', ['class' => 'btn btn-danger']) ?>
        <?= Html::endForm() ?>

        <?php if (!empty($item['submenu'])) {
            echo $this->render('_submenu', ['menu' => $item['submenu']]);
        } ?>
    </li>
    <?php endforeach; ?>

    <?php JSRegister::begin() ?>
    <script>
        $(document).on('submit', 'form.frmDelete', function() {
            return confirm('Are you sure to delete?');
        });
    </script>
    <?php JSRegister::end() ?>
</ul>
