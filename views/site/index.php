<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Menu Editor & Regex Executor!</h1>

        <p class="lead">Press a button below.</p>

        <p>
            <a class="btn btn-lg btn-success" href="<?=Yii::$app->urlManager->createUrl(['menu/index'])?>">Menu</a>
            <a class="btn btn-lg btn-default" href="<?=Yii::$app->urlManager->createUrl(['regex/index'])?>">Regex</a>
        </p>
    </div>
</div>
