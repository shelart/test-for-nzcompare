<?php

namespace app\models;

use Yii;
use yii\base\Model;

class RegexForm extends Model {
    public $input;
    public $intermediate;
    public $output;

    /** @inheritdoc */
    public function rules() {
        return [
            [['input'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'input' => 'Enter number(s) separated by comma',
            'output' => 'Processed result',
        ];
    }

    public function process() {
        $result = [];
        $intermediates = [];
        $numbers = explode(',', $this->input);
        foreach ($numbers as &$number) {
            $number = trim($number);
            $intermediates[] = preg_replace('/^(\d+)\.0+$/', '$1', $number);
            $result[] = preg_replace('/^(\d+)\.(\d+)0+$/U', '$1.$2', end($intermediates));
        }
        $this->intermediate = join(', ', $intermediates);
        $this->output = join(', ', $result);
    }
}
