<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[MenuItem]].
 *
 * @see MenuItem
 */
class MenuItemQuery extends \yii\db\ActiveQuery
{
    /** @return $this */
    public function root() {
        return $this->andWhere(['parent_id' => null]);
    }

    /**
     * {@inheritdoc}
     * @return MenuItem[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return MenuItem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
