<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu_item".
 *
 * @property int $id
 * @property string $title
 * @property string $link
 * @property int $parent_id
 *
 * @property-read MenuItem[] $children
 * @property-read MenuItem|null $parent
 */
class MenuItem extends \yii\db\ActiveRecord
{
    /** {@inheritdoc} */
    public static function tableName()
    {
        return 'menu_item';
    }

    /** @inheritdoc */
    public function transactions() {
        return array_merge(parent::transactions(), [
            self::SCENARIO_DEFAULT => [self::OP_DELETE],
        ]);
    }

    /** {@inheritdoc} */
    public function rules()
    {
        return [
            [['title', 'link'], 'required'],
            [['parent_id'], 'integer'],
            [['parent_id'], 'compare', 'compareAttribute' => 'id', 'operator' => '!='],
            [['title', 'link'], 'string', 'max' => 255],
        ];
    }

    /** {@inheritdoc} */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'link' => 'Hyperlink',
            'parent_id' => 'Parent ID',
        ];
    }

    /**
     * {@inheritdoc}
     * @return MenuItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MenuItemQuery(get_called_class());
    }

    /** @return MenuItemQuery */
    public function getChildren() {
        return $this->hasMany(MenuItem::class, ['parent_id' => 'id']);
    }

    /** @return MenuItemQuery */
    public function getParent() {
        return $this->hasOne(MenuItem::class, ['id' => 'parent_id']);
    }

    /** @inheritdoc */
    public function beforeDelete() {
        $children = $this->children;
        foreach ($children as $child) {
            if (!$child->delete()) {
                return false;
            }
        }

        return true;
    }
}
