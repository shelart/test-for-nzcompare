<?php

namespace app\controllers;

use app\models\RegexForm;
use Yii;
use yii\web\Controller;

class RegexController extends Controller
{
    public function actionIndex() {
        $model = new RegexForm();
        $model->load(Yii::$app->request->post());
        $model->process();
        return $this->render('index', [
            'model' => $model,
        ]);
    }
}
