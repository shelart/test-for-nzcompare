<?php

namespace app\controllers;

use app\models\MenuItem;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class MenuController extends Controller
{
    /** {@inheritdoc} */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param MenuItem $menuItem
     * @return array
     */
    private function _generateSubmenu($menuItem) {
        $item = [
            'id' => $menuItem->id,
            'title' => $menuItem->title,
            'link' => $menuItem->link,
            'submenu' => [],
        ];

        $children = $menuItem->getChildren()->all();
        foreach ($children as $submenuItem) {
            $submenu = $this->_generateSubmenu($submenuItem);
            $item['submenu'][$submenu['id']] = $submenu;
        }

        return $item;
    }

    /** @return array */
    private function _generateMenu() {
        $menu = [];
        $rootItems = MenuItem::find()->root()->all();
        foreach ($rootItems as $rootItem) {
            $submenu = $this->_generateSubmenu($rootItem);
            $menu[$submenu['id']] = $submenu;
        }

        return $menu;
    }

    /**
     * @param array $menuArray
     * @param integer $level Level of nesting (root is 0)
     * @return array Array consisting of one item whenever no children, or more if there are children
     */
    private function _convertMenuForDropdown($menuArray, $level) {
        $result = [
            '' => '(root)',
        ];
        foreach ($menuArray as $menuItem) {
            $result[$menuItem['id']] = str_repeat('-', $level) . ' ' . $menuItem['title'];
            $result += $this->_convertMenuForDropdown($menuItem['submenu'], $level + 1);
        }
        return $result;
    }

    public function actionIndex() {
        $menu = $this->_generateMenu();

        return $this->render('index', ['menu' => $menu]);
    }

    public function actionCreate($parentID = null) {
        $model = new MenuItem();
        if (isset($parentID)) {
            $model->parent_id = $parentID;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
            'menu' => $this->_convertMenuForDropdown($this->_generateMenu(), 0),
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        $menu = $this->_convertMenuForDropdown($this->_generateMenu(), 0);
        $menu[$id] .= " (you're editing me - can't be child of myself)";

        return $this->render('update', [
            'model' => $model,
            'menu' => $menu,
        ]);
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MenuItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MenuItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = MenuItem::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested menu item does not exist.');
    }
}
