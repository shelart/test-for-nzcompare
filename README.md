# Test Assignment by Shelekhov-Balchunas Artur for NZCompare

REQUIREMENTS
------------

You'll need a web server with PHP 7.0, MySQL/MariaDB, Composer, Git.


DEPLOY
------

### 1. Clone repo

~~~
git clone git@bitbucket.org:shelart/test-for-nzcompare.git
~~~

### 2. Prepare with Composer

~~~
cd test-for-nzcompare
composer update
~~~

### 3. Setup your web server

Assuming you have Apache, make `DocumentRoot` of your `httpd.conf` or virtual host config to point to cloned `test-for-nzcompare/web` directory. If you use another web server, please see its docs about configuring document root.

Place following lines to your `httpd.conf` after `DocumentRoot`:

~~~
DocumentRoot "YOUR_SERVER_DIR/test-for-nzcompare/web"
<Directory "YOUR_SERVER_DIR/test-for-nzcompare/web">
    Require all granted

    RewriteEngine on

    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteRule . index.php
</Directory>
~~~

Now you should be able to access the application through the following URL:

~~~
http://localhost/
~~~

### 4. Setup DB

Manually create a database with `utf8` charset and `utf8_general_ci` collation.

Edit the file `test-for-nzcompare/config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=test',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

Now apply migrations:

~~~
yii migrate
~~~
