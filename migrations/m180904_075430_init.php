<?php

use yii\db\Migration;

/** Class m180904_075430_init */
class m180904_075430_init extends Migration
{
    /** {@inheritdoc} */
    public function safeUp()
    {
        $this->createTable('menu_item', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'parent_id' => $this->integer()->null(),
        ]);
        $this->createIndex('idx_parent_id', 'menu_item', 'parent_id');
    }

    /** {@inheritdoc} */
    public function safeDown()
    {
        $this->dropTable('menu_item');
    }
}
