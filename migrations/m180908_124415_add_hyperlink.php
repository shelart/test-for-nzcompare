<?php

use yii\db\Migration;

/** Class m180908_124415_add_hyperlink */
class m180908_124415_add_hyperlink extends Migration {
    /** {@inheritdoc} */
    public function safeUp() {
        $this->addColumn('menu_item', 'link', $this->string(255)->notNull()->after('title'));
    }

    /** {@inheritdoc} */
    public function safeDown() {
        // verify there will be no data loss
        $countOfLoss = 0;
        $db = $this->getDb();
        $menuItems = $db->createCommand('SELECT * FROM menu_item')->query();
        foreach ($menuItems as $menuItem) {
            if (!empty($menuItem['link'])) {
                $countOfLoss++;
            }
        }

        if ($countOfLoss) {
            echo "{$countOfLoss} row(s) contain hyperlinks. Data would be lost if this migration is reverted!" . PHP_EOL;
            return false;
        } else {
            $this->dropColumn('menu_item', 'link');
            return true;
        }
    }
}
